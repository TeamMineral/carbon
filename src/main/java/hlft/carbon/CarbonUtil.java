package hlft.carbon;

import hlft.carbon.compat.ExtraStuff;
import hlft.carbon.init.CaBlocks;
import hlft.carbon.init.CaEnchantments;
import hlft.carbon.init.CaItems;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.eventbus.api.IEventBus;

import javax.annotation.Nonnull;

import static hlft.carbon.CarbonMain.MODID;

public class CarbonUtil {
    public static final ItemGroup TAB = new ItemGroup(MODID) {
        @Nonnull
        @Override
        public ItemStack makeIcon() {
            return CaItems.DIAMOND_COAL.get().getDefaultInstance();
        }
    };

    public static Item.Properties itemPro() {
        return new Item.Properties().tab(TAB);
    }

    public static ResourceLocation modRL(String path) {
        return new ResourceLocation(MODID, path);
    }

    public static void registerAll(IEventBus bus) {
        CaItems.ITEMS.register(bus);
        CaBlocks.BLOCKS.register(bus);
        ExtraStuff.register(bus);
        CaEnchantments.ENCHANTMENTS.register(bus);
    }

    static {
        TAB.setBackgroundImage(modRL("/textures/gui/tab_carbon.png")).hideTitle();
    }
}
