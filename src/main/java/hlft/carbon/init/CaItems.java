package hlft.carbon.init;

import hlft.carbon.CarbonMain;
import hlft.carbon.item.CommonItem;
import hlft.carbon.item.FuelItem;
import hlft.carbon.item.instance.CoalBow;
import hlft.carbon.item.instance.CoalFrame;
import hlft.carbon.item.instance.CoalItems;
import hlft.carbon.item.instance.tools.*;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public final class CaItems {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, CarbonMain.MODID);

    public static final RegistryObject<Item> COAL_FRAME = ITEMS.register(CaIDs.COAL_FRAME, CoalFrame::new);
    public static final RegistryObject<Item> DIAMOND_COAL = ITEMS.register(CaIDs.DIAMOND_COAL, CoalItems::diamond);
    public static final RegistryObject<Item> GOLD_COAL = ITEMS.register(CaIDs.GOLD_COAL, CoalItems::gold);
    public static final RegistryObject<Item> IRON_COAL = ITEMS.register(CaIDs.IRON_COAL, CoalItems::iron);
    public static final RegistryObject<Item> NETHERITE_COAL = ITEMS.register(CaIDs.NETHERITE_COAL, CoalItems::netherite);
    public static final RegistryObject<Item> LAVA_COAL = ITEMS.register("lava_" + CaIDs.COAL, () -> FuelItem.FRItem(20800));
    public static final RegistryObject<Item> WATER_COAL = ITEMS.register("water_" + CaIDs.COAL, CommonItem::new);
    public static final RegistryObject<Item> ICE_COAL = ITEMS.register("ice_" + CaIDs.COAL, CommonItem::new);

    public static final RegistryObject<BlockItem> DIAMOND_COAL_BLOCK = ITEMS.register(CaIDs.DIAMOND_COAL+"_block", CoalItems::diamondBlock);
    public static final RegistryObject<BlockItem> GOLD_COAL_BLOCK = ITEMS.register(CaIDs.GOLD_COAL+"_block", CoalItems::goldBlock);
    public static final RegistryObject<BlockItem> IRON_COAL_BLOCK = ITEMS.register(CaIDs.IRON_COAL+"_block", CoalItems::ironBlock);

    public static final RegistryObject<Item> COAL_SWORD = ITEMS.register(CaIDs.COAL+"_sword", Coal::sword);
    public static final RegistryObject<Item> CHARRED_WOODEN_SWORD = ITEMS.register(CaIDs.CHARRED_WOODEN_SWORD, Coal::charredSword);
    public static final RegistryObject<Item> COAL_SHOVEL = ITEMS.register(CaIDs.COAL+"_shovel", Coal::shovel);
    public static final RegistryObject<Item> COAL_PICKAXE = ITEMS.register(CaIDs.COAL+"_pickaxe", Coal::coalPickaxe);
    public static final RegistryObject<Item> COAL_AXE = ITEMS.register(CaIDs.COAL+"_axe", Coal::axe);
    public static final RegistryObject<Item> COAL_HOE = ITEMS.register(CaIDs.COAL+"_hoe", Coal::hoe);
    public static final RegistryObject<Item> COAL_BOW = ITEMS.register(CaIDs.COAL+"_bow", CoalBow::new);
    public static final RegistryObject<Item> COAL_CROSSBOW = ITEMS.register(CaIDs.COAL+"_crossbow", Coal::crossbow);

    public static final RegistryObject<Item> IRON_COAL_SWORD = ITEMS.register(CaIDs.IRON_COAL+"_sword", IronCoal::sword);
    public static final RegistryObject<Item> IRON_COAL_SHOVEL = ITEMS.register(CaIDs.IRON_COAL+"_shovel", IronCoal::shovel);
    public static final RegistryObject<Item> IRON_COAL_PICKAXE = ITEMS.register(CaIDs.IRON_COAL+"_pickaxe", IronCoal::pickaxe);
    public static final RegistryObject<Item> IRON_COAL_AXE = ITEMS.register(CaIDs.IRON_COAL+"_axe", IronCoal::axe);
    public static final RegistryObject<Item> IRON_COAL_HOE = ITEMS.register(CaIDs.IRON_COAL+"_hoe", IronCoal::hoe);

    public static final RegistryObject<Item> GOLD_COAL_SWORD = ITEMS.register(CaIDs.GOLD_COAL+"_sword", GoldCoal::sword);
    public static final RegistryObject<Item> GOLD_COAL_SHOVEL = ITEMS.register(CaIDs.GOLD_COAL+"_shovel", GoldCoal::shovel);
    public static final RegistryObject<Item> GOLD_COAL_PICKAXE = ITEMS.register(CaIDs.GOLD_COAL+"_pickaxe", GoldCoal::pickaxe);
    public static final RegistryObject<Item> GOLD_COAL_AXE = ITEMS.register(CaIDs.GOLD_COAL+"_axe", GoldCoal::axe);
    public static final RegistryObject<Item> GOLD_COAL_HOE = ITEMS.register(CaIDs.GOLD_COAL+"_hoe", GoldCoal::hoe);

    public static final RegistryObject<Item> DIAMOND_COAL_SWORD = ITEMS.register(CaIDs.DIAMOND_COAL+"_sword", DiamondCoal::sword);
    public static final RegistryObject<Item> DIAMOND_COAL_SHOVEL = ITEMS.register(CaIDs.DIAMOND_COAL+"_shovel", DiamondCoal::shovel);
    public static final RegistryObject<Item> DIAMOND_COAL_PICKAXE = ITEMS.register(CaIDs.DIAMOND_COAL+"_pickaxe", DiamondCoal::pickaxe);
    public static final RegistryObject<Item> DIAMOND_COAL_AXE = ITEMS.register(CaIDs.DIAMOND_COAL+"_axe", DiamondCoal::axe);
    public static final RegistryObject<Item> DIAMOND_COAL_HOE = ITEMS.register(CaIDs.DIAMOND_COAL+"_hoe", DiamondCoal::hoe);

    public static final RegistryObject<Item> NETHERITE_COAL_SWORD = ITEMS.register(CaIDs.NETHERITE_COAL+"_sword", NetheriteCoal::sword);
    public static final RegistryObject<Item> NETHERITE_COAL_SHOVEL = ITEMS.register(CaIDs.NETHERITE_COAL+"_shovel", NetheriteCoal::shovel);
    public static final RegistryObject<Item> NETHERITE_COAL_PICKAXE = ITEMS.register(CaIDs.NETHERITE_COAL+"_pickaxe", NetheriteCoal::pickaxe);
    public static final RegistryObject<Item> NETHERITE_COAL_AXE = ITEMS.register(CaIDs.NETHERITE_COAL+"_axe", NetheriteCoal::axe);
    public static final RegistryObject<Item> NETHERITE_COAL_HOE = ITEMS.register(CaIDs.NETHERITE_COAL+"_hoe", NetheriteCoal::hoe);
}
