package hlft.carbon.init;

import hlft.carbon.CarbonMain;
import hlft.carbon.enchantment.CarbonCreation;
import net.minecraft.enchantment.Enchantment;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public final class CaEnchantments {
    public static final DeferredRegister<Enchantment> ENCHANTMENTS = DeferredRegister.create(ForgeRegistries.ENCHANTMENTS, CarbonMain.MODID);

    public static final RegistryObject<Enchantment> CARBON_CREATION = ENCHANTMENTS.register(CaIDs.CARBON_CREATION, CarbonCreation::new);
}
