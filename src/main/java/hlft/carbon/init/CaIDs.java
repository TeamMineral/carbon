package hlft.carbon.init;

public final class CaIDs {
    public static final String DIAMOND_COAL = "diamond_coal";
    public static final String GOLD_COAL = "gold_coal";
    public static final String IRON_COAL = "iron_coal";
    public static final String NETHERITE_COAL = "netherite_coal";

    public static final String CRUSHED_COAL = "crushed_coal";
    public static final String COAL_FRAME = "coal_frame";
    public static final String COAL = "coal";
    public static final String CHARRED_WOODEN_SWORD = "charred_wooden_sword";
    public static final String CARBON_CREATION = "carbon_creation";
}
