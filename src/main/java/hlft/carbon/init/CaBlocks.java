package hlft.carbon.init;

import hlft.carbon.CarbonMain;
import hlft.carbon.block.CoalBlocks;
import net.minecraft.block.Block;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public final class CaBlocks {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, CarbonMain.MODID);

    public static final RegistryObject<Block> DIAMOND_COAL_BLOCK = BLOCKS.register(CaIDs.DIAMOND_COAL+"_block", CoalBlocks::diamond);
    public static final RegistryObject<Block> GOLD_COAL_BLOCK = BLOCKS.register(CaIDs.GOLD_COAL+"_block", CoalBlocks::gold);
    public static final RegistryObject<Block> IRON_COAL_BLOCK = BLOCKS.register(CaIDs.IRON_COAL+"_block", CoalBlocks::iron);
}
