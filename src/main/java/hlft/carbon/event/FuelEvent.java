package hlft.carbon.event;

import hlft.carbon.CarbonMain;
import hlft.carbon.init.CaItems;
import net.minecraft.util.IItemProvider;
import net.minecraftforge.event.furnace.FurnaceFuelBurnTimeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = CarbonMain.MODID)
public class FuelEvent {
    @SubscribeEvent
    public static void convenientFuel(FurnaceFuelBurnTimeEvent event) {
        addFuel(event, 1600*2+100, CaItems.COAL_SWORD.get());
        addFuel(event, 1600*3+100*2, CaItems.COAL_PICKAXE.get());
        addFuel(event, 1600*3+100*2, CaItems.COAL_AXE.get());
        addFuel(event, 1600+100*2, CaItems.COAL_SHOVEL.get());
        addFuel(event, 1600*2+100*2, CaItems.COAL_HOE.get());
        addFuel(event, 800, CaItems.CHARRED_WOODEN_SWORD.get());
    }

    public static void addFuel(FurnaceFuelBurnTimeEvent event, int burnTime, IItemProvider item) {
        if (event.getItemStack().getItem() == item.asItem()) {
            event.setBurnTime(burnTime);
        }
    }
}
