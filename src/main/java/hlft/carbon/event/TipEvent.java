package hlft.carbon.event;

import hlft.carbon.init.CaItems;
import net.minecraft.item.Item;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber
public class TipEvent {
    @SubscribeEvent
    public static void addItemTooltip(ItemTooltipEvent event) {
        addToolTip(event, CaItems.ICE_COAL, TextFormatting.BLUE);
        addToolTip(event, CaItems.WATER_COAL, TextFormatting.AQUA);
    }

    public static void addToolTip(ItemTooltipEvent event, RegistryObject<Item> entry) {
        Item item = entry.get().asItem();
        String description = item.getDescriptionId();
        addToolTip(event, item, new TranslationTextComponent(description + ".tooltip"));
    }

    public static void addToolTip(ItemTooltipEvent event, RegistryObject<Item> entry, TextFormatting color) {
        Item item = entry.get().asItem();
        String description = item.getDescriptionId();
        IFormattableTextComponent key = new TranslationTextComponent(description + ".tooltip").withStyle(color);
        addToolTip(event, item, (TextComponent) key);
    }

    public static void addToolTip(ItemTooltipEvent event, Item item, TextComponent text) {
        if (item == event.getItemStack().getItem()) {
            event.getToolTip().add(text);
        }
    }
}
