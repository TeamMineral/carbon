package hlft.carbon.event;

import hlft.carbon.CarbonMain;
import hlft.carbon.init.CaItems;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemModelsProperties;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, modid = CarbonMain.MODID, value = Dist.CLIENT)
public class ClientEvent {
    @SubscribeEvent
    public static void overrideRegistry(FMLClientSetupEvent event) {
        addBowOverride(event, CaItems.COAL_BOW.get());
        addCrossbowOverride(event, CaItems.COAL_CROSSBOW.get());
    }

    public static void addBowOverride(FMLClientSetupEvent event, Item item) {
        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("pulling"), (stack, world, entity) ->
                entity != null && entity.isUsingItem() && entity.getUseItem() == stack ? 1.0F : 0.0F));
        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("pull"), (stack, world, entity) -> {
            if (entity == null) {
                return 0.0F;
            } else {
                return entity.getUseItem() != stack ? 0.0F : (float)(stack.getUseDuration() - entity.getUseItemRemainingTicks()) / 20.0F;
            }
        }));
    }
    public static void addCrossbowOverride(FMLClientSetupEvent event, Item item) {
        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("pull"), (stack, world, entity) -> {
            if (entity == null) {
                return 0.0F;
            } else {
                return CrossbowItem.isCharged(stack) ? 0.0F : (float)(stack.getUseDuration() - entity.getUseItemRemainingTicks()) / (float)CrossbowItem.getChargeDuration(stack);
            }
        }));
        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("pulling"), (stack, world, entity) ->
                entity != null && entity.isUsingItem() && entity.getUseItem() == stack && !CrossbowItem.isCharged(stack) ? 1.0F : 0.0F));
        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("charged"), (stack, world, entity) ->
                entity != null && CrossbowItem.isCharged(stack) ? 1.0F : 0.0F));
        event.enqueueWork(() -> ItemModelsProperties.register(item, new ResourceLocation("firework"), (stack, world, entity) ->
                entity != null && CrossbowItem.isCharged(stack) && CrossbowItem.containsChargedProjectile(stack, Items.FIREWORK_ROCKET) ? 1.0F : 0.0F));
    }
}
