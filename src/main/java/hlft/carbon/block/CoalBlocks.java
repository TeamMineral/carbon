package hlft.carbon.block;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;

public class CoalBlocks extends Block {
    private CoalBlocks(Properties properties) {
        super(properties);
    }

    public static Block diamond() {
        return new CoalBlocks(Properties.copy(Blocks.DIAMOND_BLOCK));
    }

    public static Block gold() {
        return new CoalBlocks(Properties.copy(Blocks.GOLD_BLOCK));
    }

    public static Block iron() {
        return new CoalBlocks(Properties.copy(Blocks.IRON_BLOCK));
    }

}
