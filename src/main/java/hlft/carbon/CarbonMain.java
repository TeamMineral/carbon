package hlft.carbon;

import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(CarbonMain.MODID)
public class CarbonMain {
    public static final String MODID = "carbon";

    public CarbonMain() {
        final IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();

        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, CarbonConfig.COMMON_CONFIG);
        CarbonUtil.registerAll(bus);
    }
}