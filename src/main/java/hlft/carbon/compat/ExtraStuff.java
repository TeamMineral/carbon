package hlft.carbon.compat;

import hlft.carbon.CarbonMain;
import hlft.carbon.init.CaIDs;
import hlft.carbon.item.FuelItem;
import net.minecraft.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ExtraStuff {
    public static final DeferredRegister<Item> TCONSTRUCT_ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, CarbonMain.MODID);
    public static final DeferredRegister<Item> CREATE_ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, CarbonMain.MODID);

    public static final RegistryObject<Item> CRUSHED_COAL = CREATE_ITEMS.register(CaIDs.CRUSHED_COAL, () -> new FuelItem(2000));

    public static void register(IEventBus bus) {
        if (ModList.get().isLoaded("create")) {
            CREATE_ITEMS.register(bus);
        }
        if (ModList.get().isLoaded("tconstruct")) {
            TCONSTRUCT_ITEMS.register(bus);
        }
    }
}
