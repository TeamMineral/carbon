package hlft.carbon.item;

import hlft.carbon.CarbonUtil;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeType;

import javax.annotation.Nullable;

public class FuelItem extends Item {
    private final int burnTime;

    private FuelItem(Properties properties, int burnTime) {
        super(properties);
        this.burnTime = burnTime;
    }

    public FuelItem(int burnTime) {
        super(CarbonUtil.itemPro());
        this.burnTime = burnTime;
    }

    public static FuelItem FRItem(int burnTime) {
        return new FuelItem(CarbonUtil.itemPro().fireResistant(), burnTime);
    }

    @Override
    public int getBurnTime(ItemStack itemStack, @Nullable IRecipeType<?> recipeType) {
        return this.burnTime;
    }
}
