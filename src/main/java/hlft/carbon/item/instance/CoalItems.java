package hlft.carbon.item.instance;

import hlft.carbon.CarbonConfig;
import hlft.carbon.CarbonUtil;
import hlft.carbon.init.CaBlocks;
import hlft.carbon.item.FuelItem;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraftforge.common.ForgeConfigSpec;

import javax.annotation.Nullable;

public class CoalItems extends FuelItem {
    private CoalItems(ForgeConfigSpec.IntValue burnTime) {
        super(burnTime.get());
    }

    public static CoalItems diamond() {
        return new CoalItems(CarbonConfig.DIAMOND_COAL_VALUE);
    }

    public static CoalItems gold() {
        return new CoalItems(CarbonConfig.GOLD_COAL_VALUE);
    }

    public static CoalItems iron() {
        return new CoalItems(CarbonConfig.IRON_COAL_VALUE);
    }

    public static CoalItems netherite() {
        return new CoalItems(CarbonConfig.NETHERITE_COAL_VALUE);
    }

    public static BlockItem diamondBlock() {
        return new BlockItem(CaBlocks.DIAMOND_COAL_BLOCK.get(), CarbonUtil.itemPro()){
            @Override
            public int getBurnTime(ItemStack itemStack, @Nullable IRecipeType<?> recipeType) {
                return CarbonConfig.DIAMOND_COAL_VALUE.get()*9;
            }
        };
    }

    public static BlockItem goldBlock() {
        return new BlockItem(CaBlocks.GOLD_COAL_BLOCK.get(), CarbonUtil.itemPro()){
            @Override
            public int getBurnTime(ItemStack itemStack, @Nullable IRecipeType<?> recipeType) {
                return CarbonConfig.GOLD_COAL_VALUE.get()*9;
            }
        };
    }

    public static BlockItem ironBlock() {
        return new BlockItem(CaBlocks.IRON_COAL_BLOCK.get(), CarbonUtil.itemPro()){
            @Override
            public int getBurnTime(ItemStack itemStack, @Nullable IRecipeType<?> recipeType) {
                return CarbonConfig.IRON_COAL_VALUE.get()*9;
            }
        };
    }
}
