package hlft.carbon.item.instance;

import hlft.carbon.CarbonUtil;
import hlft.carbon.init.CaItems;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class CoalFrame extends Item {
    public CoalFrame() {
        super(CarbonUtil.itemPro().stacksTo(16));
    }

    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        ItemStack itemstack = player.getItemInHand(hand);
        BlockRayTraceResult result = getPlayerPOVHitResult(world, player, RayTraceContext.FluidMode.SOURCE_ONLY);
        ActionResult<ItemStack> ret = net.minecraftforge.event.ForgeEventFactory.onBucketUse(player, world, itemstack, result);
        if (ret != null) return ret;
        if (result.getType() == RayTraceResult.Type.MISS) {
            return ActionResult.pass(itemstack);
        } else if (result.getType() != RayTraceResult.Type.BLOCK) {
            return ActionResult.pass(itemstack);
        } else {
            if (!world.isClientSide()) {
                BlockPos blockpos = result.getBlockPos();
                if (world.getBlockState(blockpos).is(Blocks.LAVA)) {
                    itemstack.shrink(1);
                    player.inventory.add(new ItemStack(CaItems.LAVA_COAL.get()));
                    world.playSound(null, blockpos, SoundEvents.BUCKET_FILL_LAVA, SoundCategory.PLAYERS, 1.0F, 1.0F);
                    return ActionResult.success(itemstack);
                } else if (world.getBlockState(blockpos).is(Blocks.WATER)) {
                    itemstack.shrink(1);
                    player.inventory.add(new ItemStack(CaItems.WATER_COAL.get()));
                    world.playSound(null, blockpos, SoundEvents.BUCKET_FILL, SoundCategory.PLAYERS, 1.0F, 1.0F);
                    return ActionResult.success(itemstack);
                }
            }
        }
        return ActionResult.pass(itemstack);
    }
}
