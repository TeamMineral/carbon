package hlft.carbon.item.instance.tools;

import hlft.carbon.CarbonUtil;
import hlft.carbon.init.CaItems;
import net.minecraft.item.*;
import net.minecraft.item.crafting.Ingredient;

public class DiamondCoal {
    public static final IItemTier DIAMOND_COAL = new IItemTier() {
        @Override
        public int getUses() {
            return 1856;
        }

        @Override
        public float getSpeed() {
            return 10.0F;
        }

        @Override
        public float getAttackDamageBonus() {
            return 3.3F;
        }

        @Override
        public int getLevel() {
            return 3;
        }

        @Override
        public int getEnchantmentValue() {
            return 8;
        }

        @Override
        public Ingredient getRepairIngredient() {
            return Ingredient.of(CaItems.DIAMOND_COAL.get());
        }
    };

    public static SwordItem sword() {
        return new SwordItem(DIAMOND_COAL, 3, -2.4F, CarbonUtil.itemPro());
    }

    public static PickaxeItem pickaxe() {
        return new PickaxeItem(DIAMOND_COAL, 1, -2.8F, CarbonUtil.itemPro());
    }

    public static AxeItem axe() {
        return new AxeItem(DIAMOND_COAL, 6.0F, -3.1F, CarbonUtil.itemPro());
    }

    public static HoeItem hoe() {
        return new HoeItem(DIAMOND_COAL, -2, -1.0F, CarbonUtil.itemPro());
    }

    public static ShovelItem shovel() {
        return new ShovelItem(DIAMOND_COAL, 1.5F, -3.0F, CarbonUtil.itemPro());
    }
}
