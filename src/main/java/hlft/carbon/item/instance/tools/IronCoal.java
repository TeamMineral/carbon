package hlft.carbon.item.instance.tools;

import hlft.carbon.CarbonUtil;
import hlft.carbon.init.CaItems;
import net.minecraft.item.*;
import net.minecraft.item.crafting.Ingredient;

public class IronCoal {
    public static final IItemTier IRON_COAL = new IItemTier() {
        @Override
        public int getUses() {
            return 450;
        }

        @Override
        public float getSpeed() {
            return 6.75F;
        }

        @Override
        public float getAttackDamageBonus() {
            return 2.2F;
        }

        @Override
        public int getLevel() {
            return 2;
        }

        @Override
        public int getEnchantmentValue() {
            return 4;
        }

        @Override
        public Ingredient getRepairIngredient() {
            return Ingredient.of(CaItems.IRON_COAL.get());
        }
    };

    public static SwordItem sword() {
        return new SwordItem(IRON_COAL, 3, -2.4F, CarbonUtil.itemPro());
    }

    public static PickaxeItem pickaxe() {
        return new PickaxeItem(IRON_COAL, 1, -2.8F, CarbonUtil.itemPro());
    }

    public static AxeItem axe() {
        return new AxeItem(IRON_COAL, 6.0F, -3.1F, CarbonUtil.itemPro());
    }

    public static HoeItem hoe() {
        return new HoeItem(IRON_COAL, -2, -1.0F, CarbonUtil.itemPro());
    }

    public static ShovelItem shovel() {
        return new ShovelItem(IRON_COAL, 1.5F, -3.0F, CarbonUtil.itemPro());
    }
}
