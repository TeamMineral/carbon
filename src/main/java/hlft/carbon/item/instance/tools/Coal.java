package hlft.carbon.item.instance.tools;

import hlft.carbon.CarbonUtil;
import net.minecraft.item.*;
import net.minecraft.item.crafting.Ingredient;

public class Coal {
    public static final IItemTier COAL = new IItemTier() {
        @Override
        public int getUses() {
            return 203;
        }

        @Override
        public float getSpeed() {
            return 5F;
        }

        @Override
        public float getAttackDamageBonus() {
            return 1.4F;
        }

        @Override
        public int getLevel() {
            return 2;
        }

        @Override
        public int getEnchantmentValue() {
            return 3;
        }

        @Override
        public Ingredient getRepairIngredient() {
            return Ingredient.of(Items.COAL, Items.CHARCOAL);
        }
    };

    public static SwordItem sword() {
        return new SwordItem(COAL, 3, -2.4F, CarbonUtil.itemPro());
    }

    public static SwordItem charredSword() {
        return new SwordItem(COAL, 4, -2.8F, CarbonUtil.itemPro());
    }

    public static PickaxeItem coalPickaxe() {
        return new PickaxeItem(COAL, 1, -2.8F, CarbonUtil.itemPro());
    }

    public static AxeItem axe() {
        return new AxeItem(COAL, 6.0F, -3.1F, CarbonUtil.itemPro());
    }

    public static HoeItem hoe() {
        return new HoeItem(COAL, -2, -1.0F, CarbonUtil.itemPro());
    }

    public static ShovelItem shovel() {
        return new ShovelItem(COAL, 1.5F, -3.0F, CarbonUtil.itemPro());
    }

    public static CrossbowItem crossbow() {
        return new CrossbowItem(CarbonUtil.itemPro().stacksTo(1).durability(503));
    }
}
