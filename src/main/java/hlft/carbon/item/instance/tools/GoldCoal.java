package hlft.carbon.item.instance.tools;

import hlft.carbon.CarbonUtil;
import hlft.carbon.init.CaItems;
import net.minecraft.item.*;
import net.minecraft.item.crafting.Ingredient;

public class GoldCoal {
    public static final IItemTier GOLD_COAL = new IItemTier() {
        @Override
        public int getUses() {
            return 60;
        }

        @Override
        public float getSpeed() {
            return 15.0F;
        }

        @Override
        public float getAttackDamageBonus() {
            return 0.5F;
        }

        @Override
        public int getLevel() {
            return 2;
        }

        @Override
        public int getEnchantmentValue() {
            return 16;
        }

        @Override
        public Ingredient getRepairIngredient() {
            return Ingredient.of(CaItems.GOLD_COAL.get());
        }
    };

    public static SwordItem sword() {
        return new SwordItem(GOLD_COAL, 3, -2.4F, CarbonUtil.itemPro());
    }

    public static PickaxeItem pickaxe() {
        return new PickaxeItem(GOLD_COAL, 1, -2.8F, CarbonUtil.itemPro());
    }

    public static AxeItem axe() {
        return new AxeItem(GOLD_COAL, 6.0F, -3.1F, CarbonUtil.itemPro());
    }

    public static HoeItem hoe() {
        return new HoeItem(GOLD_COAL, -2, -1.0F, CarbonUtil.itemPro());
    }

    public static ShovelItem shovel() {
        return new ShovelItem(GOLD_COAL, 1.5F, -3.0F, CarbonUtil.itemPro());
    }
}
