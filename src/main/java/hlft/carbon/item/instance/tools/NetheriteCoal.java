package hlft.carbon.item.instance.tools;

import hlft.carbon.CarbonUtil;
import hlft.carbon.init.CaItems;
import net.minecraft.item.*;
import net.minecraft.item.crafting.Ingredient;

public class NetheriteCoal {
    public static final IItemTier IRON_COAL = new IItemTier() {
        @Override
        public int getUses() {
            return 2531;
        }

        @Override
        public float getSpeed() {
            return 10F;
        }

        @Override
        public float getAttackDamageBonus() {
            return 4.5F;
        }

        @Override
        public int getLevel() {
            return 4;
        }

        @Override
        public int getEnchantmentValue() {
            return 10;
        }

        @Override
        public Ingredient getRepairIngredient() {
            return Ingredient.of(CaItems.NETHERITE_COAL.get());
        }
    };

    public static SwordItem sword() {
        return new SwordItem(IRON_COAL, 3, -2.4F, itemPro());
    }

    public static PickaxeItem pickaxe() {
        return new PickaxeItem(IRON_COAL, 1, -2.8F, itemPro());
    }

    public static AxeItem axe() {
        return new AxeItem(IRON_COAL, 6.0F, -3.1F, itemPro());
    }

    public static HoeItem hoe() {
        return new HoeItem(IRON_COAL, -2, -1.0F, itemPro());
    }

    public static ShovelItem shovel() {
        return new ShovelItem(IRON_COAL, 1.5F, -3.0F, itemPro());
    }

    private static Item.Properties itemPro() {
        return new Item.Properties().tab(CarbonUtil.TAB).fireResistant();
    }
}
