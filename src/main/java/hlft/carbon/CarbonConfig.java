package hlft.carbon;

import net.minecraftforge.common.ForgeConfigSpec;

public class CarbonConfig {
    public static ForgeConfigSpec COMMON_CONFIG;
    public static ForgeConfigSpec.IntValue DIAMOND_COAL_VALUE;
    public static ForgeConfigSpec.IntValue GOLD_COAL_VALUE;
    public static ForgeConfigSpec.IntValue IRON_COAL_VALUE;
    public static ForgeConfigSpec.IntValue NETHERITE_COAL_VALUE;

    static {
        ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();

        builder.comment("Fuel settings").push("fuel");
        DIAMOND_COAL_VALUE = fuelValue(builder, "DIAMOND_COAL", 4000);
        GOLD_COAL_VALUE = fuelValue(builder, "GOLDEN_COAL", 3200);
        IRON_COAL_VALUE = fuelValue(builder, "IRON_COAL", 2400);
        NETHERITE_COAL_VALUE = fuelValue(builder, "NETHERITE_COAL", 8000);
        builder.pop();

        COMMON_CONFIG = builder.build();
    }

    private static ForgeConfigSpec.IntValue fuelValue(ForgeConfigSpec.Builder builder, String s, int defaultValue) {
        return builder.comment(optimizeString(s) + "'s burn time").defineInRange(s.toLowerCase(), defaultValue, 0, Integer.MAX_VALUE);
    }

    private static String optimizeString(String s) {
        char[] ch = s.replace('_', ' ').toLowerCase().toCharArray();
        if (ch[0] >= 'a' && ch[0] <= 'z') {
            ch[0] = (char) (ch[0] - 32);
        }
        return new String(ch);
    }
}
