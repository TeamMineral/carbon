# Carbon
# 现仓库已经转为https://github.com/Renascence-Studio/Carbon
# 本仓库已经弃用
#### 模组说明
本模组为 [煤炭工艺](https://www.mcmod.cn/class/3356.html) 的重置版，  
模组开发中...   
重置进度：  
- 各材料煤炭物品 (√)
- 各材料煤炭方块 (√)
- 煤炭工具装备 (×)

#### 使用说明
Mod Loader: Forge  
MC版本：1.16.5 
